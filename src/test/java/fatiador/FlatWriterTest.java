package fatiador;

import static fatiador.PersonBuilder.aPerson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class FlatWriterTest {

    @Test
    public void shouldWriteSimpleFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);

        Person person = new Person("12345678901", "Leonardo");

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo  ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        Person person = new Person("12345678901", "Leonardo");
        person.setDogs("rex", "dino", "milu");

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  3rex  dino milu ";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteIntegerSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleIntegerField("age", 2);

        Person person = aPerson("12345678901", "Leonardo").withAge(29).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  29";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteIntMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        Person person = new Person("12345678901", "Leonardo");
        person.setScores(12, 13, 14);

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  03121314";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteDecimalSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        Person person = aPerson("12345678901", "Leonardo").withWeight(75.99).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  07599";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteAllFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);
        personStructure.addSimpleIntegerField("age", 2);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        Person person = aPerson("12345678901", "Leonardo").withAge(29).withWeight(75.99).build();
        person.setScores(12, 13, 14);
        person.setDogs("rex", "dino", "milu");

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  031213143rex  dino milu 2907599";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteNullValues() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleIntegerField("age", 2);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        Person person = new Person("12345678901", null);
        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901          0000000";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteValorDecimalMenorQueTamanhoDecimal() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        Person person = aPerson("12345678901", "Leonardo  ").withWeight(70.1).build();
        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  07010";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteListasVazias() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        Person person = aPerson("12345678901", "Leonardo").build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  000";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteListasNulas() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        Person person = aPerson("12345678901", "Leonardo").build();
        person.scores = null;
        person.dogNames = null;

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);

        String expectedFlat = "12345678901Leonardo  000";
        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteSimpleDateField() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        LocalDate birthday = LocalDate.of(1987, 06, 07);
        Person person = aPerson("12345678901", "Leonardo").withBirthday(birthday).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo  19870607";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteSimpleDateFieldWithNullDate() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        Person person = aPerson("12345678901", "Leonardo").withBirthday(null).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo          ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldNotWriteSimpleDateFieldWithInvalidDateFormat() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        String invalidDateFormat = "YYYYmmDD";
        personStructure.addSimpleDateField("birthday", invalidDateFormat);

        LocalDate birthday = LocalDate.of(1987, 06, 07);
        Person person = aPerson("12345678901", "Leonardo").withBirthday(birthday).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);

        try {
            writer.write(person);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains(invalidDateFormat));
            assertTrue(e.getMessage().contains("birthday"));
        }
    }

    @Test
    public void shouldWriteSimpleTimeField() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleTimeField("birthTime", "HHmmss");

        LocalTime birthTime = LocalTime.of(17, 15, 43);
        Person person = aPerson("12345678901", "Leonardo").withBirthTime(birthTime).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901Leonardo  171543";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteToPrivateFields() {

        FlatStructure bookStructure = new FlatStructure();
        bookStructure.addSimpleAlphaField("title", 15);
        bookStructure.addSimpleAlphaField("author", 15);

        Book book = new Book("War and Peace", "Leo Tolstoy");

        FlatWriter<Book> writer = new FlatWriter<>(bookStructure, Book.class);
        String flat = writer.write(book);
        String expectedFlat = "War and Peace  Leo Tolstoy    ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToFalse() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "0", "1");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.FALSE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   0";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToFalseText() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "false", "true");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.FALSE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   false";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToFalseTextUppercase() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "FALSE", "TRUE");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.FALSE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   FALSE";

        assertEquals(expectedFlat, flat);
    }


    @Test
    public void shouldWriteBooleanFieldToTrue() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "0", "1");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.TRUE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   1";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToTrueText() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "false", "true");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.TRUE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   true ";

        assertEquals(expectedFlat, flat);
    }

    @Test
    public void shouldWriteBooleanFieldToTrueTextUppercase() {
        FlatStructure personStructure = new FlatStructure();

        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addBooleanField("isAdult", "FALSE", "TRUE");

        Person person = aPerson("12345678901", "CryBabe").withIsAdult(Boolean.TRUE).build();

        FlatWriter<Person> writer = new FlatWriter<>(personStructure, Person.class);
        String flat = writer.write(person);
        String expectedFlat = "12345678901CryBabe   TRUE ";

        assertEquals(expectedFlat, flat);
    }

}
