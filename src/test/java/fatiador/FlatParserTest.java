package fatiador;

import static fatiador.PersonBuilder.aPerson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class FlatParserTest {

    @Test
    public void shouldParseSimpleFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);

        String flatInput = "12345678901Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        String flatInput = "12345678901Leonardo  3rex  dino milu ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        expectedPerson.setDogs("rex", "dino", "milu");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseIntegerSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleIntegerField("age", 2);

        String flatInput = "12345678901Leonardo  29";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = aPerson("12345678901", "Leonardo").withAge(29).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseIntMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        String flatInput = "12345678901Leonardo  03121314";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        expectedPerson.setScores(12, 13, 14);
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseDecimalSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        String flatInput = "12345678901Leonardo  07599";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo", 75.99);
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseAllFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);
        personStructure.addSimpleIntegerField("age", 2);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        String flatInput = "12345678901Leonardo  031213143rex  dino milu 2907599";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo", 29, 75.99);
        expectedPerson.setScores(12, 13, 14);
        expectedPerson.setDogs("rex", "dino", "milu");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseListasVazias() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        String flatInput = "12345678901Leonardo  000";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = aPerson("12345678901", "Leonardo").build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldThrowExceptionWhenIntegerFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleIntegerField("age", 2);

        String flatInput = "12345678901Leonardo  2A";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains(FlatParser.FIELD_NOT_PARSABLE_PREFIX));
        }
    }

    @Test
    public void shouldThrowExceptionWhenMultipleIntegerFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        String flatInput = "12345678901Leonardo  03122A13";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains(FlatParser.FIELD_NOT_PARSABLE_PREFIX));
        }
    }

    @Test
    public void shouldThrowExceptionWhenLengthFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        String flatInput = "12345678901Leonardo  A3121314";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains(FlatParser.FIELD_NOT_PARSABLE_PREFIX));
        }
    }

    @Test
    public void shouldThrowExceptionWhenDoubleFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        String flatInput = "12345678901Leonardo  02A12";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains(FlatParser.FIELD_NOT_PARSABLE_PREFIX));
        }
    }

    @Test
    public void shouldParseStructuredMultipleField() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addSimpleAlphaField("name", 5);
        dogStructure.addSimpleAlphaField("breed", 10);

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("dogs", 2);
        personStructure.addMultipleStructuredField("dogs", dogStructure, Dog.class);

        String flatInput = "12345678901Leonardo  02Rex  Husky     Totó Poodle    ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        Dog expectedDog1 = new Dog();
        expectedDog1.name = "Rex";
        expectedDog1.breed = "Husky";
        expectedPerson.dogs.add(expectedDog1);
        Dog expectedDog2 = new Dog();
        expectedDog2.name = "Totó";
        expectedDog2.breed = "Poodle";
        expectedPerson.dogs.add(expectedDog2);

        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        String flatInput = "12345678901Leonardo  19870607";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalDate birthday = LocalDate.of(1987, 06, 07);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthday(birthday).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldNotParseSimpleDateFieldWithInvalidDate() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        String invalidDate = "19871340";
        String flatInput = "12345678901Leonardo  " + invalidDate;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains(invalidDate));
            assertTrue(e.getMessage().contains("birthday"));
        }
    }

    @Test
    public void shouldNotParseSimpleDateFieldWithInvalidDateFormat() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        String invalidDateFormat = "YYYYmmDD";
        personStructure.addSimpleDateField("birthday", invalidDateFormat);

        String flatInput = "12345678901Leonardo  19870607";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains(invalidDateFormat));
            assertTrue(e.getMessage().contains("birthday"));
        }
    }

    @Test
    public void shouldParseSimpleTimeField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleTimeField("birthTime", "HHmmss");

        String flatInput = "12345678901Leonardo  160834";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalTime birthTime = LocalTime.of(16, 8, 34);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthTime(birthTime).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseWritingOnPrivateFields() {

        FlatStructure bookStructure = new FlatStructure();
        bookStructure.addSimpleAlphaField("title", 15);
        bookStructure.addSimpleAlphaField("author", 15);

        String flatInput = "War and Peace  Leo Tolstoy    ";

        FlatParser<Book> parser = new FlatParser<>(bookStructure, Book.class);
        Book book = parser.parse(flatInput);

        Book expectedBook = new Book("War and Peace", "Leo Tolstoy");
        assertEquals(expectedBook, book);
    }

    @Test
    public void shouldParseSimpleDateFieldToNullWhenFlatDateIsBlank() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        String dateBlank = "        ";

        String flatInput = "12345678901Leonardo  " + dateBlank;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthday(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateFieldToNullWhenFlatDateIsZeros() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        String dateZeros = "00000000";

        String flatInput = "12345678901Leonardo  " + dateZeros;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthday(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleTimeFieldToNullWhenFlatTimeIsBlank() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleTimeField("birthTime", "HHmmss");

        String blankTime = "      ";

        String flatInput = "12345678901Leonardo  " + blankTime;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthTime(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseBooleanFieldToTrue() {

        FlatStructure personStructure = new FlatStructure();
        String flatTrueValue = "1";
        String flatFalseValue = "0";
        personStructure.addBooleanField("isAdult", flatTrueValue, flatFalseValue);
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);

        String flatInput = "1";
        Person person = parser.parse(flatInput);

        assertTrue(person.isAdult);
    }
    
    @Test
    public void shouldParseBooleanFieldToFalse() {

        FlatStructure personStructure = new FlatStructure();
        String flatTrueValue = "1";
        String flatFalseValue = "0";
        personStructure.addBooleanField("isAdult", flatTrueValue, flatFalseValue);
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        
        String flatInput = "0";
        Person person = parser.parse(flatInput);
        
        assertFalse(person.isAdult);
    }
    
    @Test
    public void shouldNotParseInvalidBooleanField() {

        FlatStructure personStructure = new FlatStructure();
        String flatTrueValue = "1";
        String flatFalseValue = "0";
        personStructure.addBooleanField("isAdult", flatTrueValue, flatFalseValue);
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);

        String flatInput = "7";

        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("isAdult"));
            assertTrue(e.getMessage().contains("7"));
            assertTrue(e.getMessage().contains("0"));
            assertTrue(e.getMessage().contains("1"));
        }

    }
}
