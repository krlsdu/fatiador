package fatiador;

import static org.junit.Assert.*;

import org.junit.Test;

public class FlatStructureTest {

    @Test
    public void shouldReturnStructureLength() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addSimpleAlphaField("name", 5);
        dogStructure.addSimpleAlphaField("breed", 10);
        dogStructure.addSimpleIntegerField("age", 2);
        dogStructure.addSimpleDecimalField("weight", 4, 2);

        assertEquals(21, dogStructure.length());
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotCalculateLengthOfStructureWithMultipleField() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addSimpleAlphaField("name", 5);
        dogStructure.addSimpleAlphaField("breed", 10);
        dogStructure.addSimpleIntegerField("age", 2);
        dogStructure.addSimpleDecimalField("weight", 4, 2);
        dogStructure.addLengthField("fleasNames", 2);
        dogStructure.addMultipleAlphaField("fleasNames", 10);

        dogStructure.length();
    }
}
