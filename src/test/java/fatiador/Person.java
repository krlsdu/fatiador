package fatiador;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Person {

    public String identificationNumber;
    public String name;
    public LocalDate birthday;
    public LocalTime birthTime;
    public Integer age;
    public List<String> dogNames = new ArrayList<>();
    public List<Integer> scores = new ArrayList<>();
    public Double weight;
    public List<Dog> dogs = new ArrayList<>();
    public Boolean isAdult;

    public Person() {

    }

    public Person(String identificationNumber, String name) {
        this.identificationNumber = identificationNumber;
        this.name = name;
    }

    public Person(String identificationNumber, String name, Integer age) {
        this.identificationNumber = identificationNumber;
        this.name = name;
        this.age = age;
    }

    public Person(String identificationNumber, String name, double weight) {
        this.identificationNumber = identificationNumber;
        this.name = name;
        this.weight = weight;
    }

    public Person(String identificationNumber, String name, int age, double weight) {
        this.identificationNumber = identificationNumber;
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public void setScores(Integer... scores) {
        this.scores = Arrays.asList(scores);
    }

    public void setDogs(String... dogs) {
        this.dogNames = Arrays.asList(dogs);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((age == null) ? 0 : age.hashCode());
        result = prime * result + ((birthTime == null) ? 0 : birthTime.hashCode());
        result = prime * result + ((birthday == null) ? 0 : birthday.hashCode());
        result = prime * result + ((identificationNumber == null) ? 0 : identificationNumber.hashCode());
        result = prime * result + ((dogNames == null) ? 0 : dogNames.hashCode());
        result = prime * result + ((dogs == null) ? 0 : dogs.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((scores == null) ? 0 : scores.hashCode());
        result = prime * result + ((weight == null) ? 0 : weight.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        if (age == null) {
            if (other.age != null)
                return false;
        } else if (!age.equals(other.age))
            return false;
        if (birthTime == null) {
            if (other.birthTime != null)
                return false;
        } else if (!birthTime.equals(other.birthTime))
            return false;
        if (birthday == null) {
            if (other.birthday != null)
                return false;
        } else if (!birthday.equals(other.birthday))
            return false;
        if (identificationNumber == null) {
            if (other.identificationNumber != null)
                return false;
        } else if (!identificationNumber.equals(other.identificationNumber))
            return false;
        if (dogNames == null) {
            if (other.dogNames != null)
                return false;
        } else if (!dogNames.equals(other.dogNames))
            return false;
        if (dogs == null) {
            if (other.dogs != null)
                return false;
        } else if (!dogs.equals(other.dogs))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (scores == null) {
            if (other.scores != null)
                return false;
        } else if (!scores.equals(other.scores))
            return false;
        if (weight == null) {
            if (other.weight != null)
                return false;
        } else if (!weight.equals(other.weight))
            return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Person [identificationNumber=");
        builder.append(identificationNumber);
        builder.append(", name=");
        builder.append(name);
        builder.append(", birthday=");
        builder.append(birthday);
        builder.append(", birthTime=");
        builder.append(birthTime);
        builder.append(", age=");
        builder.append(age);
        builder.append(", dogNames=");
        builder.append(dogNames);
        builder.append(", scores=");
        builder.append(scores);
        builder.append(", weight=");
        builder.append(weight);
        builder.append(", dogs=");
        builder.append(dogs);
        builder.append("]");
        return builder.toString();
    }

}
