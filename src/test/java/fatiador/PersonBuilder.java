package fatiador;

import java.time.LocalDate;
import java.time.LocalTime;

public class PersonBuilder {

    private Person person;

    public PersonBuilder(String identificationNumber, String name) {
        this.person = new Person(identificationNumber, name);
    }

    public static PersonBuilder aPerson(String identificationNumber, String name) {
        PersonBuilder builder = new PersonBuilder(identificationNumber, name);
        return builder;
    }

    public PersonBuilder withAge(Integer age) {
        person.age = age;
        return this;
    }

    public PersonBuilder withWeight(Double weight) {
        person.weight = weight;
        return this;
    }

    public PersonBuilder withBirthday(LocalDate birthday) {
        person.birthday = birthday;
        return this;
    }

    public PersonBuilder withBirthTime(LocalTime birthTime) {
        person.birthTime = birthTime;
        return this;
    }

    public PersonBuilder withIsAdult(Boolean isAdult) {
        person.isAdult = isAdult;
        return this;
    }

    public Person build() {
        return person;
    }

}
