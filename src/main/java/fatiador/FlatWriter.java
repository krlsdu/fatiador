package fatiador;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Collection;
import java.util.Collections;

import com.google.common.base.Strings;
import org.apache.commons.lang3.BooleanUtils;

/**
 * Use this class to convert objects of type T into flat strings.
 */
public class FlatWriter<T> {

    public static final String FIELD_NOT_WRITABLE_PREFIX = "I couldn't read the field ";

    private FlatStructure structure;
    private Class<T> productClass;

    public FlatWriter(FlatStructure structure, Class<T> productClass) {
        this.structure = structure;
        this.productClass = productClass;
    }

    public String write(T bean) {
        try {
            return _write(bean);
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @SuppressWarnings("rawtypes")
    private String _write(T bean) throws Exception {

        StringBuilder flat = new StringBuilder();

        for (FlatField flatField : structure.getFields()) {

            String name = flatField.getName();
            int size = flatField.getSize();
            FlatType flatType = flatField.getFlatType();
            Field javaField = productClass.getDeclaredField(name);
            javaField.setAccessible(true);

            if (flatField instanceof SimpleField) {
                if (flatType == FlatType.ALPHA) {
                    String rawValue = (String) javaField.get(bean);
                    String flatValue = Strings.padEnd(Strings.nullToEmpty(rawValue), size, ' ');
                    flat.append(flatValue);
                }
                if (flatType == FlatType.INTEGER) {
                    Integer rawValue = (Integer) javaField.get(bean);
                    Integer intValue = rawValue != null ? rawValue : 0;
                    String flatValue = Strings.padStart(intValue.toString(), size, '0');
                    flat.append(flatValue);
                }
                if (flatType == FlatType.DECIMAL) {
                    int decimalDigitsSize = flatField.getDecimalDigitsSize();
                    Double rawValue = (Double) javaField.get(bean);
                    Double doubleValue = rawValue != null ? rawValue : 0;
                    String integerPart = doubleValue.toString().split("\\.")[0];
                    String decimalPart = doubleValue.toString().split("\\.")[1];
                    integerPart = Strings.padStart(integerPart, size - decimalDigitsSize, '0');
                    decimalPart = Strings.padEnd(decimalPart, decimalDigitsSize, '0');
                    decimalPart = decimalPart.substring(0, decimalDigitsSize);
                    flat.append(integerPart + decimalPart);
                }
                if (flatType == FlatType.DATE) {
                    LocalDate rawValue = (LocalDate) javaField.get(bean);
                    String pattern = flatField.getDateTimeFormat();
                    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(pattern);
                    String flatValue;
                    if (rawValue != null) {
                        try {
                            flatValue = rawValue.format(dateFormat);
                        } catch (UnsupportedTemporalTypeException e) {
                            String msg = FIELD_NOT_WRITABLE_PREFIX + "[" + name + "] with the value [" + rawValue
                                    + "] using the pattern [" + pattern + "].";
                            throw new IllegalArgumentException(msg, e);
                        }
                    } else {
                        flatValue = Strings.padEnd("", pattern.length(), ' ');
                    }
                    flat.append(flatValue);
                }
                if (flatType == FlatType.TIME) {
                    LocalTime rawValue = (LocalTime) javaField.get(bean);
                    String pattern = flatField.getDateTimeFormat();
                    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(pattern);
                    String flatValue;
                    if (rawValue != null) {
                        try {
                            flatValue = rawValue.format(dateFormat);
                        } catch (UnsupportedTemporalTypeException e) {
                            String msg = FIELD_NOT_WRITABLE_PREFIX + "[" + name + "] with the value [" + rawValue
                                    + "] using the pattern [" + pattern + "].";
                            throw new IllegalArgumentException(msg, e);
                        }
                    } else {
                        flatValue = Strings.padEnd("", pattern.length(), ' ');
                    }
                    flat.append(flatValue);
                }

                if (flatType == FlatType.BOOLEAN) {
                    Boolean rawValue = (Boolean) javaField.get(bean);
                    String flatValue;
                        flatValue = Strings.padEnd(BooleanUtils.toString(rawValue, flatField.getFlatFalse(), flatField.getFlatTrue()), size, ' ');
                        flat.append(flatValue);
                }
            }

            if (flatField instanceof LengthField) {
                Collection collection = (Collection) javaField.get(bean);
                if (collection == null) {
                    collection = Collections.emptyList();
                }
                Integer value = collection.size();
                String flatValue = Strings.padStart(value.toString(), size, '0');
                flat.append(flatValue);
            }

            if (flatField instanceof MultipleField) {
                Collection collection = (Collection) javaField.get(bean);
                if (collection == null) {
                    collection = Collections.emptyList();
                }
                for (Object element : collection) {
                    if (flatType == FlatType.ALPHA) {
                        String rawValue = (String) element;
                        String flatValue = Strings.padEnd(Strings.nullToEmpty(rawValue), size, ' ');
                        flat.append(flatValue);
                    }
                    if (flatType == FlatType.INTEGER) {
                        Integer rawValue = (Integer) element;
                        String flatValue = Strings.padStart(rawValue.toString(), size, '0');
                        flat.append(flatValue);
                    }
                }
            }
        }

        return flat.toString();
    }

}
