package fatiador;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;

import com.google.common.base.Splitter;

/**
 * Use this class to convert flat strings into objects of type T.
 */
public class FlatParser<T> {

    public static final String FIELD_NOT_PARSABLE_PREFIX = "I couldn't store on field ";

    private FlatStructure structure;
    private Class<T> productClass;

    private Map<String, Integer> quantityOfChunksOfMultipleFields = new HashMap<>();

    public FlatParser(FlatStructure structure, Class<T> productClass) {
        this.structure = structure;
        this.productClass = productClass;
    }

    public T parse(String flatInput) {
        try {
            return _parse(flatInput);
        } catch (ParserArgumentException e) {
            throw new IllegalArgumentException(e.getMessage(), e.getCause());
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private T _parse(String flatInput) throws Exception {

        T bean = productClass.newInstance();

        int pos = 0;
        for (FlatField flatField : structure.getFields()) {

            String name = flatField.getName();
            int size = flatField.getSize();
            FlatType flatType = flatField.getFlatType();

            if (flatField instanceof SimpleField) {
                Field javaField = productClass.getDeclaredField(name);
                javaField.setAccessible(true);
                String value = flatInput.substring(pos, pos + size).trim();
                if (flatType == FlatType.ALPHA) {
                    javaField.set(bean, value);
                }
                if (flatType == FlatType.INTEGER) {
                    Integer intValue;
                    try {
                        intValue = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        String msg = FIELD_NOT_PARSABLE_PREFIX + "[" + name + "] the value [" + value + "].";
                        throw new ParserArgumentException(msg, e);
                    }
                    javaField.set(bean, intValue);
                }
                if (flatType == FlatType.DECIMAL) {
                    double doubleValue;
                    try {
                        doubleValue = Double.parseDouble(value);
                    } catch (NumberFormatException e) {
                        String msg = FIELD_NOT_PARSABLE_PREFIX + "[" + name + "] the value [" + value + "].";
                        throw new ParserArgumentException(msg, e);
                    }
                    int decimalDigitsSize = flatField.getDecimalDigitsSize();
                    javaField.set(bean, doubleValue / Math.pow(10, decimalDigitsSize));
                }
                if (flatType == FlatType.DATE) {

                    if (!(value.trim().isEmpty() || value.replace('0', ' ').trim().isEmpty())) {

                        LocalDate dateValue;
                        String pattern = flatField.getDateTimeFormat();
                        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(pattern);
                        try {
                            dateValue = LocalDate.parse(value, dateFormat);
                        } catch (DateTimeParseException e) {
                            String msg = FIELD_NOT_PARSABLE_PREFIX + "[" + name + "] the value [" + value
                                    + "] using the pattern [" + pattern + "].";
                            throw new ParserArgumentException(msg, e);
                        }

                        javaField.set(bean, dateValue);

                    }

                }
                if (flatType == FlatType.TIME) {

                    if (!(value.trim().isEmpty())) {

                        LocalTime timeValue;
                        String pattern = flatField.getDateTimeFormat();
                        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(pattern);
                        try {
                            timeValue = LocalTime.parse(value, dateFormat);
                        } catch (DateTimeParseException e) {
                            String msg = FIELD_NOT_PARSABLE_PREFIX + "[" + name + "] the value [" + value
                                    + "] using the pattern [" + pattern + "].";
                            throw new ParserArgumentException(msg, e);
                        }

                        javaField.set(bean, timeValue);

                    }

                }

                if (flatType == FlatType.BOOLEAN) {
                    String flatTrue = flatField.getFlatTrue();
                    String flatFalse = flatField.getFlatFalse();
                    try {
                        javaField.set(bean, BooleanUtils.toBoolean(value, flatTrue, flatFalse));
                    } catch (IllegalArgumentException e) {
                        String msg = FIELD_NOT_PARSABLE_PREFIX + "[" + name + "] the boolean value of [" + value
                                + "] for True = [" + flatTrue + "] and False = [" + flatFalse + "].";
                        throw new ParserArgumentException(msg, e);
                    }
                }

                pos += size;
            }

            if (flatField instanceof LengthField) {
                String value = flatInput.substring(pos, pos + size);
                int intValue;
                try {
                    intValue = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    String msg = FIELD_NOT_PARSABLE_PREFIX + " length of [" + name + "] the value [" + value + "].";
                    throw new ParserArgumentException(msg, e);
                }
                quantityOfChunksOfMultipleFields.put(name, intValue);
                pos += size;
            }

            if (flatField instanceof MultipleField) {
                Integer quantityOfChunks = quantityOfChunksOfMultipleFields.get(name);
                Field javaField = productClass.getDeclaredField(name);
                javaField.setAccessible(true);
                Collection collection = (Collection) javaField.get(bean);
                String flatList = flatInput.substring(pos, pos + quantityOfChunks * size);
                if (!flatList.isEmpty()) {
                    if (flatType == FlatType.ALPHA) {
                        Splitter.fixedLength(size).split(flatList).forEach(chunk -> collection.add(chunk.trim()));
                    }
                    if (flatType == FlatType.INTEGER) {
                        try {
                            Splitter.fixedLength(size).split(flatList)
                                    .forEach(chunk -> collection.add(Integer.parseInt(chunk)));
                        } catch (NumberFormatException e) {
                            String msg = FIELD_NOT_PARSABLE_PREFIX + "[" + name + "] the value [" + flatList + "].";
                            throw new ParserArgumentException(msg, e);
                        }
                    }
                    if (flatType == FlatType.STRUCTURED) {
                        Class subBeanClass = flatField.getBeanClass();
                        FlatStructure subStructure = flatField.getStructure();
                        Splitter.fixedLength(size).split(flatList)
                                .forEach(chunk -> collection.add(objectFromChunk(chunk, subBeanClass, subStructure)));
                    }
                }
                pos += quantityOfChunks * size;
            }

        }

        return bean;
    }

    private Object objectFromChunk(String chunk, Class beanClass, FlatStructure subStructure) {
        FlatParser parser = new FlatParser<>(subStructure, beanClass);
        Object subBean = parser.parse(chunk);
        return subBean;
    }

}
