package fatiador;

public abstract class FlatField {

    private FlatType flatType = FlatType.ALPHA;
    private Class<?> beanClass;
    private String name;
    private int size;
    private int decimalDigitsSize = 0;
    private String dateTimeFormat;
    private FlatStructure structure;
    private String flatTrue;
    private String flatFalse;

    public FlatField(String name, int size) {
        this.name = name;
        this.size = size;
        this.beanClass = flatType.beanClass();
    }

    public FlatField(String name, int size, FlatType flatType) {
        this.name = name;
        this.size = size;
        this.flatType = flatType;
        this.beanClass = flatType.beanClass();
    }

    public FlatField(String name, int size, int decimalDigitsSize) {
        this.name = name;
        this.size = size;
        this.decimalDigitsSize = decimalDigitsSize;
        this.flatType = FlatType.DECIMAL;
        this.beanClass = flatType.beanClass();
    }

    public FlatField(String name, FlatStructure structure, Class<?> beanClass) {
        this.name = name;
        this.structure = structure;
        this.flatType = FlatType.STRUCTURED;
        this.beanClass = beanClass;
    }

    public FlatField(String name, String dateTimeFormat, FlatType flatType) {
        this.name = name;
        this.dateTimeFormat = dateTimeFormat;
        this.size = dateTimeFormat.length();
        this.flatType = flatType;
        this.beanClass = flatType.beanClass();
    }

    public FlatField(String name, int size, String flatTrue, String flatFalse) {
        this.name = name;
        this.size = size;
        this.flatTrue = flatTrue;
        this.flatFalse = flatFalse;
        this.flatType = FlatType.BOOLEAN;
        this.beanClass = FlatType.BOOLEAN.beanClass();
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        if (flatType == FlatType.STRUCTURED) {
            return structure.length();
        } else {
            return size;
        }
    }

    public FlatType getFlatType() {
        return flatType;
    }

    public int getDecimalDigitsSize() {
        return decimalDigitsSize;
    }

    public String getDateTimeFormat() {
        return dateTimeFormat;
    }

    public FlatStructure getStructure() {
        return structure;
    }

    public String getFlatTrue() {
        return flatTrue;
    }

    public String getFlatFalse() {
        return flatFalse;
    }

    public Class<?> getBeanClass() {
        return beanClass;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FlatField [flatType=");
        builder.append(flatType);
        builder.append(", beanClass=");
        builder.append(beanClass);
        builder.append(", name=");
        builder.append(name);
        builder.append(", size=");
        builder.append(size);
        builder.append(", decimalDigitsSize=");
        builder.append(decimalDigitsSize);
        builder.append(", dateTimeFormat=");
        builder.append(dateTimeFormat);
        builder.append(", structure=");
        builder.append(structure);
        builder.append(", flatTrue=");
        builder.append(flatTrue);
        builder.append(", flatFalse=");
        builder.append(flatFalse);
        builder.append("]");
        return builder.toString();
    }

}
