package fatiador;

import java.util.ArrayList;
import java.util.List;

/**
 * Use this class to define the structure of your flat string. Think of a
 * FlatStructure as a Cobol copybook ;)
 *
 */
public class FlatStructure {

    private List<FlatField> fields = new ArrayList<>();

    public List<FlatField> getFields() {
        return fields;
    }

    public void addSimpleAlphaField(String name, int size) {
        fields.add(new SimpleField(name, size));
    }

    public void addSimpleIntegerField(String name, int size) {
        fields.add(new SimpleField(name, size, FlatType.INTEGER));
    }

    public void addSimpleDecimalField(String name, int size, int decimalDigitsSize) {
        fields.add(new SimpleField(name, size, decimalDigitsSize));
    }

    public void addMultipleAlphaField(String name, int size) {
        fields.add(new MultipleField(name, size));
    }

    public void addMultipleIntegerField(String name, int size) {
        fields.add(new MultipleField(name, size, FlatType.INTEGER));
    }

    public void addLengthField(String name, int size) {
        fields.add(new LengthField(name, size));
    }

    public void addMultipleStructuredField(String name, FlatStructure structure, Class<?> beanClass) {
        fields.add(new MultipleField(name, structure, beanClass));
    }

    public int length() {
        if (structureHasAnyMultipleField()) {
            throw new IllegalStateException(
                    "It's not possible to calculate the length of a structure with a field of multiple values");
        }
        return fields.stream().mapToInt(f -> f.getSize()).sum();
    }

    private boolean structureHasAnyMultipleField() {
        return fields.stream().anyMatch(f -> f instanceof MultipleField);
    }

    public void addSimpleDateField(String name, String dateFormat) {
        fields.add(new SimpleField(name, dateFormat, FlatType.DATE));
    }

    public void addSimpleTimeField(String name, String timeFormat) {
        fields.add(new SimpleField(name, timeFormat, FlatType.TIME));
    }

    public void addBooleanField(String name, String flatTrueValue, String flatFalseValue) {
        int size = Math.max(flatTrueValue.length(), flatFalseValue.length()); 
        fields.add(new SimpleField(name, size, flatTrueValue, flatFalseValue));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FlatStructure [fields=");
        builder.append(fields);
        builder.append("]");
        return builder.toString();
    }

}
