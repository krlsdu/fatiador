package fatiador;

import java.time.LocalDate;
import java.time.LocalTime;

public enum FlatType {

	ALPHA(String.class), 
	INTEGER(Integer.class), 
	DECIMAL(Double.class),
	DATE(LocalDate.class),
	TIME(LocalTime.class),
	STRUCTURED(null),
	BOOLEAN(Boolean.class);

	private Class<?> beanClass;
	
	private FlatType(Class<?> beanClass) {
		this.beanClass = beanClass;
	}
	
	public Class<?> beanClass() {
		return beanClass;
	}
}
